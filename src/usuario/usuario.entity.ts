import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'usuarios' })
export class UsuarioEntity {
  @PrimaryGeneratedColumn()
  idUsuario: number;

  @Column({
    length: 30,
  })
  public nombreUsuario: string;

  @Column({
    length: 50,
  })
  public apellidoUsuario: string;

  @Column({
    length: 50,
  })
  public username: string;

  @Column({
    length: 250,
    name: 'password',
  })
  public password_hash: string;
}
