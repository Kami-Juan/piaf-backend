import { ApiModelProperty } from '@nestjs/swagger';

export class LoginUsuarioDto {
  @ApiModelProperty()
  readonly username: string;

  @ApiModelProperty()
  readonly password: string;
}
