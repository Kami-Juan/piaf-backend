import { ApiModelProperty } from '@nestjs/swagger';

export class CrearUsuarioDto {
  @ApiModelProperty()
  readonly nombreUsuario: string;

  @ApiModelProperty()
  readonly apellidoUsuario: string;

  @ApiModelProperty()
  readonly username: string;

  @ApiModelProperty()
  readonly password: string;
}
