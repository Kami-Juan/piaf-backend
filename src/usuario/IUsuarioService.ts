import { CrearUsuarioDto } from './dto/crear-usuario.dto';
import { UsuarioEntity } from './usuario.entity';

export interface IUsuarioService {
  obtenerUsuarios(): Promise<UsuarioEntity[]>;
  crearUsuario(usuario: CrearUsuarioDto): Promise<UsuarioEntity>;
}
