import {
  Controller,
  Get,
  UseGuards,
  Request,
  Post,
  Delete,
  Patch,
  Body,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioService } from './usuario.service';
import { CrearUsuarioDto } from './dto/crear-usuario.dto';

@Controller('usuarios')
export class UsuarioController {
  constructor(private readonly usuarioService: UsuarioService) {}

  @Get('')
  @UseGuards(AuthGuard('jwt'))
  obtenerUsuarios(@Request() request) {
    return [];
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  obtenerUsuario(@Request() request) {
    return [];
  }

  @Post('')
  // @UseGuards(AuthGuard('jwt'))
  async crearUsuario(@Body() usuarioDto: CrearUsuarioDto) {
    return await this.usuarioService.crearUsuario(usuarioDto);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  actualizarUsuario(@Request() request) {
    return [];
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  eliminarUsuario(@Request() request) {
    return [];
  }
}
