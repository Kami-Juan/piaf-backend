import { Injectable, BadGatewayException } from '@nestjs/common';
import { IUsuarioService } from './IUsuarioService';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioEntity } from './usuario.entity';
import { CrearUsuarioDto } from './dto/crear-usuario.dto';
import { Repository } from 'typeorm';
import { JwtPayload } from '../passport/jwt-payload.interface';
import * as bcrypt from 'bcryptjs';
import { LoginUsuarioDto } from './dto/login-usuario.dto';

@Injectable()
export class UsuarioService implements IUsuarioService {
  constructor(
    @InjectRepository(UsuarioEntity)
    private readonly usuarioRepository: Repository<UsuarioEntity>,
  ) {}

  async obtenerUsuarios(): Promise<UsuarioEntity[]> {
    return this.usuarioRepository.find();
  }

  async obtenerUsuarioLogin(loginDto: LoginUsuarioDto): Promise<UsuarioEntity> {
    return this.usuarioRepository.findOne({
      where: { username: loginDto.username },
    });
  }

  async crearUsuario(usuariodto: CrearUsuarioDto): Promise<UsuarioEntity> {
    try {
      let usuarioEntity: UsuarioEntity = new UsuarioEntity();

      usuarioEntity.apellidoUsuario = usuariodto.apellidoUsuario;
      usuarioEntity.nombreUsuario = usuariodto.nombreUsuario;
      usuarioEntity.username = usuariodto.username;

      let salt: string = await bcrypt.genSalt(15);
      usuarioEntity.password_hash = await bcrypt.hash(
        usuariodto.password,
        salt,
      );

      return await this.usuarioRepository.save(usuarioEntity);
    } catch (e) {
      throw new BadGatewayException(e);
    }
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return await this.usuarioRepository.findOne(payload.id);
  }
}
