import { Test, TestingModule } from '@nestjs/testing';
import { ParcelaController } from './parcela.controller';

describe('Parcela Controller', () => {
  let controller: ParcelaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ParcelaController],
    }).compile();

    controller = module.get<ParcelaController>(ParcelaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
