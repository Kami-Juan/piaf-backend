import { Controller, Post, Get, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUsuarioDto } from '../usuario/dto/login-usuario.dto';
import { UsuarioService } from '../usuario/usuario.service';
import { UsuarioEntity } from '../usuario/usuario.entity';
import * as bcrypt from 'bcryptjs';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usuarioService: UsuarioService,
  ) {}

  @Post('login')
  public async login(@Body() loginDto: LoginUsuarioDto) {
    const usuario: UsuarioEntity = await this.usuarioService.obtenerUsuarioLogin(
      loginDto,
    );

    if (!usuario) {
      // Create exceptions
      return;
    }

    const isValidPassword: boolean = await bcrypt.compare(
      loginDto.password,
      usuario.password_hash,
    );

    if (!isValidPassword) {
      // Create exceptions
      return;
    }

    return this.authService.signIn(loginDto);
  }
}
