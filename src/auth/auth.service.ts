import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsuarioService } from '../usuario/usuario.service';
import { JwtPayload } from '../passport/jwt-payload.interface';
import { UsuarioEntity } from '../usuario/usuario.entity';

import { LoginUsuarioDto } from '../usuario/dto/login-usuario.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usuarioService: UsuarioService,
    private readonly jwtService: JwtService,
  ) {}

  async signIn(payload: LoginUsuarioDto): Promise<string> {
    return this.jwtService.sign(payload);
  }

  async validarUsuario(payload: JwtPayload): Promise<UsuarioEntity> {
    return await this.usuarioService.validateUser(payload);
  }
}
