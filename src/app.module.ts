import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ParcelaModule } from './parcela/parcela.module';
import { RegistroParcelaModule } from './registro-parcela/registro-parcela.module';
import { RegadoModule } from './regado/regado.module';
import { UsuarioModule } from './usuario/usuario.module';
import { AuthModule } from './auth/auth.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ParcelaModule,
    RegistroParcelaModule,
    RegadoModule,
    UsuarioModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
